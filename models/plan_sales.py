# -*- coding: utf-8 -*-

from odoo import models, fields


class PlanSales(models.Model):
    _name = "sales_developer.plan.sale"

    service = fields.Char(string="Servicios")
    price_plan = fields.Char(string="Precio del plan")