# -*- coding: utf-8 -*-

from odoo import models, fields


class PrepaidSales(models.Model):
    _name = "sales_developer.prepaid.sale"

    product_prepaid = fields.Char(string="Producto Almacenable")
    serial_number = fields.Char(string="Numero de serie")
    contract_number = fields.Char(string="Numero de contrato")
    price = fields.Char(string="Precio")

