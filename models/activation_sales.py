# -*- coding: utf-8 -*-

from odoo import models, fields


class ActivationSales(models.Model):
    _name = "sales_developer.activation.sale"

    services = fields.Char(string="Servicios")
    product_store = fields.Char(string="Productos alamcenables")
    serial_number = fields.Char(string="Numero de serie")
    contract_number = fields.Char(string="Numero de Contrato")
    price_rent = fields.Char(string="Renta mensual")
    proteccion_movil = fields.Char(string="Proteccion de equipo")