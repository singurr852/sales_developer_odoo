# -*- coding: utf-8 -*-
{
    'name': "Ventas Developer",

    'summary': """Venta Prepago, Venta Plan y Venta Activacion""",

    'author': "E&G",
    'website': "http://www.yourcompany.com",

    'category': 'Test',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/groups.xml',
        'security/ir.model.access.csv',
        'views/prepaid_sales_view.xml',
        'views/plan_sales_view.xml',
        'views/activation_sales_view.xml',

    ],
    'installable': True,
    'application': True,
}